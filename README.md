# fNIRS Learning

<img align="right" width="100" height="100" src="logo.png">

**Title:** fNIRS Learning  
**Keywords:** fnirs, machine learning, deep learning  
**Version:** 0.1  
**Authors:** Johann Benerradi, Jeremie Clos  
**Web site:** [Mixed Reality Lab](https://www.nottingham.ac.uk/research/groups/mixedrealitylab/)  
**Platform:** Python 3  
**License:** GNU GPL 3.0  

### Description
Machine learning processing for fNIRS  

### Requires
- [Python 3](https://www.python.org/downloads/)  
- [Jupyter](http://jupyter.org/)
- [NumPy](http://www.numpy.org/)  
- [Scikit-learn](http://scikit-learn.org/stable/)  
- [PyTorch](https://pytorch.org/)

### Citation
[**Exploring Machine Learning Approaches for Classifying Mental Workload using fNIRS Data from HCI Tasks**](https://dl.acm.org/doi/abs/10.1145/3363384.3363392)  
*Johann Benerradi, Horia A. Maior, Adrian  Marinescu, Jeremie  Clos and Max L. Wilson*  

BibTeX:
```
@inproceedings{benerradi2019exploring,
  title={Exploring Machine Learning Approaches for Classifying Mental Workload using fNIRS Data from HCI Tasks},
  author={Benerradi, Johann and A. Maior, Horia and Marinescu, Adrian and Clos, Jeremie and L. Wilson, Max},
  booktitle={Proceedings of the Halfway to the Future Symposium 2019},
  pages={1--11},
  year={2019}
}
```

---
&copy; *2018-2020* [*Johann Benerradi*](https://gitlab.com/HanBnrd)
